<?php
/**
 * Class SUPER_UPDATES
 *
 * Get remote data from a Bitbucket repo.
 *
 * @package SP\SUPER_UPDATES
 * @author  YOUR NAME
 */

namespace SSP\SUPER_UPDATES;
use stdClass;
use LicenseKeys\Utility\Api;
use LicenseKeys\Utility\Client;
use LicenseKeys\Utility\LicenseRequest;

/*
 * Exit if called directly.
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}

class SUPER_UPDATES {

	/**
	 * Setup variables
	*/
	public $args = array();
	public $plugin_slug = '';
	public $plugin_file = '';
	public $plugin_data = '';
	public $option_key = '';
	public $sku = '';
	public $license_url = '';
	public $store_code = '';
	public $license_field;
	public $text_domain = '';
	public $license_key = '';
	public $activation_id = '';
	public $expire_date = '';

	/**
	 * Construct the class
	*/
	function __construct( $args = array() ) {
		$this->args = $args;

		//= Do stuff to build our class
		$fields = array(
			'version',
			'plugin_name',
			'prefix',
			'plugin_slug',
			'plugin_file',
			'plugin_data',
			'option_key',
			'license_url',
			'sku',
			'store_code',
			'license_field',
			'text_domain',
			'license_key',
			'activation_id',
			'general_settings_page',
		);
		foreach( $fields as $field ) {
			if ( isset( $args[ $field ] ) ) {
				$this->$field = $args[ $field ];
			}
		}
		
		return $this;
	}

	public function init() {
		if ( is_admin() ) {
			$this->admin_custom_scripts();
			if ( $this->general_settings_page ) {
				$this->admin_init();
			}
		}
		add_filter( 'site_transient_update_plugins', array( $this, 'extend_filter_update_plugins' ) );
		add_filter( 'transient_update_plugins', array( $this, 'extend_filter_update_plugins' ) );
//		add_filter( 'plugins_api_result', array( $this, 'plugin_info' ), 20, 3);
//		add_filter( 'plugins_api', array( $this, 'plugin_info' ), 20, 3);
		add_action( 'upgrader_process_complete', array( $this, 'after_update' ), 10, 2 );
		add_action( 'wp_ajax_' . $this->prefix . '_ajax_handler', array( $this, 'ajax_handler' ) );
		add_action( 'wp_ajax_nopriv_' . $this->prefix . '_ajax_handler', array( $this, 'ajax_handler' ) );
	}

	/* Add our admin scripts & styles */
	public function admin_custom_scripts() {
		$pluginpath = plugins_url() . '/' . $this->plugin_slug . '/bin/';
		wp_enqueue_style( $this->prefix . 'u_admin_css', $pluginpath . 'css/bin.css', false, $this->version );
		wp_register_script( $this->prefix . 'u_admin_js', $pluginpath . 'js/bin.js', array( 'jquery' ), $this->version );
		wp_localize_script( $this->prefix . 'u_admin_js', 'spAdminParams', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'prefix' => $this->prefix,
		) );
		wp_enqueue_script( $this->prefix . 'u_admin_js' );
	}

	public function extend_filter_update_plugins( $update_plugins ) {
		//= check license
		$valid = $this->is_valid();
		if ( ! $valid ) {
			delete_transient( 'custom_update_' . $this->plugin_slug );
			return;
		}

		if ( ! is_object( $update_plugins ) )
				return $update_plugins;

		if ( ! isset( $update_plugins->response ) || ! is_array( $update_plugins->response ) )
			$update_plugins->response = array();

		// Do whatever you need to see if there's a new version of your plugin
		// Your response will need to look something like this if it's out of date:

		$info = self::plugin_info( '' );
		
		$plugin_path = plugin_dir_path(dirname(__FILE__));
		$plugin_path .= $this->plugin_slug . '.php';
		if ( ! file_exists( $plugin_path ) ) {
			return;
		}
		$plugin_data = get_plugin_data( $plugin_path );
		$version = $plugin_data['Version'];
		$new_version = $info->new_version;
		if ( $version >= $new_version ) {
			return false;
		}
		$update_plugins->response[ $this->plugin_file ] = (object)$info;

		return $update_plugins;
	}

	/*
	 * $res empty at this step
	 * $action 'plugin_information'
	 * $args stdClass Object ( [slug] => woocommerce [is_ssl] => [fields] => Array ( [banners] => 1 [reviews] => 1 [downloaded] => [active_installs] => 1 ) [per_page] => 24 [locale] => en_US )
	 */
	public function plugin_info( $args ) {
		// trying to get from cache first
		if( false == $remote = get_transient( 'custom_update_' . $this->plugin_slug ) ) {
			// the file with the actual plugin information on your server
			$remote = wp_remote_get( $this->plugin_data, array(
				'timeout' => 20,
				'headers' => array(
					'Accept' => 'application/json'
				) )
			);
			if ( ! is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && ! empty( $remote['body'] ) ) {
				set_transient( 'custom_update_' . $this->plugin_slug, $remote, 43200 ); // 12 hours cache
			}
		}
		//https://rudrastyh.com/wordpress/self-hosted-plugin-update.html
/*
		if( ! is_wp_error( $remote ) 
		   && isset( $remote['response']['code'] ) 
		   && $remote['response']['code'] == 200 
		   && ! empty( $remote['body'] ) 
		   && ! empty( $remote['license'] ) ) {
*/
		if( ! is_wp_error( $remote ) 
		   && isset( $remote['response']['code'] ) 
		   && $remote['response']['code'] == 200 
		   && ! empty( $remote['body'] ) ) {

			$body = $remote['body'];
			$body = trim( $body, "\0" );
//			$body = stripslashes( html_entity_decode( $body ) );
			$remote = json_decode( $body );
			
			//= TO DO - if the new_version is higher than current version
			
			$res = new stdClass();
			$res->name = $remote->name;

			if ( isset( $remote->plugin ) ) {
				$res->plugin = $remote->plugin;
			}
			if ( isset( $remote->version ) ) {
				$res->version = $remote->version;
			}
			if ( isset( $remote->tested ) ) {
				$res->tested = $remote->tested;
			}
			if ( isset( $remote->requires ) ) {
				$res->requires = $remote->requires;
			}
			if ( isset( $remote->author ) ) {
				$res->author = $remote->author;
			}
			if ( isset( $remote->author_profile ) ) {
				$res->author_profile = $remote->author_profile;
			}
			if ( isset( $remote->download_url ) ) {
				$res->download_link = $remote->download_url;
			}
			if ( isset( $remote->trunk ) ) {
				$res->trunk = $remote->download_url;
			}
			$res->requires_php = '5.3';
			if ( isset( $remote->last_updated ) ) {
				$res->last_updated = $remote->last_updated;
			}
			$res->sections = array(
				'description' => $remote->sections->description,
				'installation' => $remote->sections->installation,
				'changelog' => $remote->sections->changelog,
				// you can add your custom sections (tabs) here
			);
			if ( isset( $remote->icon ) ) {
				$res->icon = $remote->icon;
			}
			if ( isset( $remote->rating ) ) {
				$res->rating = $remote->rating;
			}
			if ( isset( $remote->ratings ) ) {
				$res->ratings = $remote->ratings;
			}
			if ( isset( $remote->num_ratings ) ) {
				$res->num_ratings = $remote->num_ratings;
			}
			if ( isset( $remote->active_installs ) ) {
				$res->active_installs = $remote->active_installs;
			}
			if ( isset( $remote->slug ) ) {
				$res->slug = $remote->slug;
			}
			if ( isset( $remote->new_version ) ) {
				$res->new_version = $remote->new_version;
			}

			// in case you want the screenshots tab, use the following HTML format for its content:
			// <ol><li><a href="IMG_URL" target="_blank"><img src="IMG_URL" alt="CAPTION" /></a><p>CAPTION</p></li></ol>
			if( !empty( $remote->sections->screenshots ) ) {
				$res->sections['screenshots'] = $remote->sections->screenshots;
			}

			$res->banners = array(
				'low' => $remote->banners->low,
				'high' => $remote->banners->high
			);

			if ( empty( $res->download_link ) ) {
				if ( isset( $this->args['download_url'] ) ) {
					$res->download_link = $this->args['download_url'];
				}
			}
			$res->package = $this->args['download_url'];

			return $res;
		}

		return false;
	}

	public function after_update( $upgrader_object, $options ) {
		if ( $options['action'] == 'update' && $options['type'] === 'plugin' )  {
			// just clean the cache when new plugin version is installed
			delete_transient( 'custom_update_' . $this->plugin_slug );
		}
	}
	
	/*******************************************************************/
	//= Start License Code
	//= https://github.com/10quality/license-keys-php-client/wiki/Tutorial:-Quick-Script
	/*******************************************************************/

	/**
	 * Method used to activate the product. Returns true or false.
	 * @param string $license_key A customer license key.
	 * @return bool
	 */
	public function activate( $license_key ) {
		$license_response = function() use( $license_key ) {
			//= create($license_url, $store_code, $sku, $license_key, $frequency = self::DAILY_FREQUENCY, $handler = null)
			return LicenseRequest::create(
				$this->license_url,
				$this->store_code,
				$this->sku,
				$license_key,
				LicenseRequest::DAILY_FREQUENCY
			);
		};

		$response = Api::activate(
			Client::instance(),
			$license_response,
			function( $license ) {
				update_option( $this->option_key, $license, true );
			}
		);
		
		$activation_id = $response->data->activation_id;
		$expire_date = $response->data->expire_date;
		update_option( $this->option_key . '_activation_id', $activation_id, true );
		update_option( $this->option_key . '_expire_date', $expire_date, true );

		return $response;
	}

	/**
	 * Returns true or false.
	 * @return bool
	 */
	public function is_valid() {
		return Api::softValidate(
			function() {
				$response = new LicenseRequest( get_option( $this->option_key ) );
				
				$this->activation_id = get_option( $this->option_key . '_activation_id', false );
				$this->expire_date = get_option( $this->option_key . '_expire_date', false );

				return $response;
			}
		);
	}

	/**
	 * Returns true or false.
	 * @return bool
	 */
	public function deactivate() {
		$license_response = function() use( $license_key ) {
			//= create($license_url, $store_code, $sku, $license_key, $frequency = self::DAILY_FREQUENCY, $handler = null)
			return LicenseRequest::create(
				$this->license_url,
				$this->store_code,
				$this->sku,
				$license_key,
				LicenseRequest::DAILY_FREQUENCY
			);
		};
		$response = Api::deactivate(
			Client::instance(),
			$license_response,
			function( $license ) {
				if ($license === null) {
					update_option( $this->option_key . '_license', '' );
				}
			}
		);
		return $response->error === false;
	}

	/*******************************************************************/
	//= Admin Settings
	/*******************************************************************/
	/* Settings Init */
	private function admin_init() {
		//==============================================================
		/* Create settings section */
		add_settings_section(
			$this->plugin_slug . '-id',             						// Section ID
			$this->plugin_name . __( ' License', $this->text_domain ),		// Section title
			array( $this, 'description' ),									// Section callback function
			'general'                          								// Settings page slug
		);

		//==============================================================
		/* Register Settings */
		/* ALL SETTINGS MUST BE REGISTERED */
		register_setting(
			'general',       	      					// Options group
			$this->option_key . '_license',				// Option name/database
			array( $this, 'settings_sanitize' ) 		// sanitize callback function
		);

		//============================================================== 
		/* Actually Create settings field */
		add_settings_field(
			$this->option_key . '_license',    							// Field ID
			$this->plugin_name . __( ' License', $this->text_domain ),	// Field title 
			array( $this, 'settings_callback' ),						// Field callback function
			'general',                    								// Settings page slug
			$this->plugin_slug . '-id'          						// Section ID
		);

	}

	public function description() {
		$html = __( 'This settings allow you to manage your license for ' . $this->plugin_name . '.', $this->text_domain );
		echo wpautop( $html );
	}

	/* Sanitize Callback Function */
	public function settings_sanitize( $input ) {
		return sanitize_text_field( $input );
	}

	/* Settings Field Callback */
	public function settings_callback() {
		$setting_slug = $this->option_key . '_license';

		$html = '';
		$nonce = wp_create_nonce( $this->prefix . '_nonce' );
        $array = (array)$this;
        $json = json_encode( $array, JSON_UNESCAPED_SLASHES );
        $base64 = base64_encode( $json );
		
		$activation_id = get_option( $this->option_key . '_activation_id', '' );
		$class = '';
		$button_text = __( 'Activate', $this->text_domain );
		if ( ! empty( $activation_id ) ) {
			$class = 'active-license';
			$button_text = __( 'Deactivate', $this->text_domain );
		}

        //$html .= '<div class="sp-license-wrap sp-license-wrap-' . $this->plugin_slug . '" data-base64="' . $base64 . '" data-json=\'' . $json . '\'>';
		$html .= '<div class="' . $this->prefix . '-license-wrap ' . $this->prefix . '-license-wrap-' . $this->plugin_slug . '" data-base64="' . $base64 . '">';
			$html .= '<input type="hidden" name="' . $this->prefix . '_nonce" id="' . $this->prefix . '_nonce" value="' . $nonce . '" />';
			$html .= '<label for="' . $setting_slug . '">';
				$html .= '<input class="' . $class . '" type="text" id="' . $this->plugin_slug . '-license" name="' . $setting_slug . '" value="' . get_option( $setting_slug, '' ) . '" />';
				$html .= '<br />';
				$html .= '<small>' . __( 'Enter your license key here.', $this->text_domain ) . '</small>';
				$html .= '<br />';
				$html .= '<input type="button" value="' . $button_text . '" class="' . $this->prefix . '-validate-license" data-slug="' . $this->plugin_slug . '" data-prefix="' . $this->prefix . '" />';
				$html .= '<div class="' . $this->prefix . '-updates-license-msg ' . $this->prefix . '-updates-license-msg-' . $this->plugin_slug . '" data-slug="' . $this->plugin_slug . '" data-prefix="' . $this->prefix . '">';
				$html .= '</div>';
			$html .= '</label>';
		$html .='</div>';

		echo $html;
	}

	/*******************************************************************/
	//= Start ajax Handler
	/*******************************************************************/
	function ajax_handler() {
		if ( ! is_admin() ) {
			return;
		}
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		$nonce = $_POST[ 'nonce'];

		if ( ! wp_verify_nonce( $nonce, $this->prefix . '_nonce' ) ) {
			echo __( 'Nonce check failed - no changes saved.', $this->text_domain );
			exit();
		}

		$return = array();
		if ( isset( $_POST['key'] ) ) {
			$return['license'] = sanitize_text_field( $_POST['key'] );
			update_option( $this->option_key, $return['license'], true );
		}

		$return['slug'] = sanitize_text_field( $_POST['slug'] );
		if ( ! isset( $return['license'] ) ) {
			$return['license'] = get_option(  $this->option_key . '_license', '' );
		}
		$return['plugin_name'] = $this->plugin_name;
		
		$return['method'] = sanitize_text_field( $_POST['method'] );
		if ( 'activate' === $return['method'] ) {
			$return['response'] = $this->activate( $return['license'] );
		} else {
			$return['response'] = $this->deactivate();
			$return['response']['message'] = __( 'Deactivated', $this->text_domain );
			delete_option( $this->option_key . '_activation_id' );
			delete_option( $this->option_key . '_expire_date' );
		}
		$json = json_encode( $return );
		echo $json;
		exit();
	}
	/*******************************************************************/
	//= Stop ajax Handler
	/*******************************************************************/
	
	/*******************************************************************/
	//= Start Utilities
	/*******************************************************************/
	/**
	 * Attempt to get request IP
	 * Just a handy function that sometimes comes in use
	 *
	*/
	private function request_ip() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		if ( strpos( $ip, ',' ) !== false ) {
			$ips = explode( ',', $ip );
			$ip = $ips[0];
		}
		
		return $ip;
	}

	/**
	 * Save log items to WordPress debug.log
	 *
	 * @param $log - line to record in log file
	*/
    private function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( 'SUPER_UPDATES: ' . print_r( $log, true ) );
            } else {
                error_log( 'SUPER_UPDATES: ' . $log );
            }
        }
    }

}
?>