<?php
/*
 * Start bin
*/
use SSP\SUPER_UPDATES\SUPER_UPDATES;

function super_simple_popup_args() {
	$version = SSP_VERSION;
	$plugin_name = 'Super Simple Popup';
	$prefix = 'ssp';
	$option_key = '_super_simple_popup';
	$slug = 'super-simple-popup';
	$license_field = 'ssp_license';
	
	$store_code = 'idIqleL68v8EgRV';
	$license = get_option(  $option_key . '_license', '' );
	if ( empty( $license ) ) {
//		return;
	}
	$activation_id = get_option(  $option_key . '_activation_id', '' );

	$plugin_file = 'super-simple-popup/super-simple-popup.php';
	$plugin_data = 'https://wpsuperplugins.com/validation/' . $license . '/?sku=' . $slug . '&action=details';

	$args = array(
		'version' => $version,
		'plugin_name' => $plugin_name,
		'prefix' => $prefix,
		'option_key' => $option_key,
		'plugin_slug' => $slug,
		'plugin_file' => $plugin_file,
		'plugin_data' => $plugin_data,
		'license_url' => 'https://wpsuperplugins.com/wp-admin/admin-ajax.php',
		'sku' => $slug,
		'store_code' => $store_code,
		'license_field' => $license_field,
		'text_domain' => $slug,
		'license_key' => $license,
		'activation_id' => $activation_id,
		'download_url' => 'https://wpsuperplugins.com/validation/' . $license . '/?sku=' . $slug . '&action=download',
		'general_settings_page' => false,
	);
	return $args;
}

//= Unique init function name specific to this plugin
//= This function name must be different for every plugin
add_action( 'admin_init', 'super_simple_popup_init' );
function super_simple_popup_init() {
	$args = super_simple_popup_args();

	if ( ! class_exists( 'SSP\SUPER_UPDATES\SUPER_UPDATES' ) ) {
		require_once( 'class-update.php' );
		if ( ! class_exists( 'LicenseKeys\Utility\Api' ) ) {
			require_once( 'class-api.php' );
		}
		if ( ! class_exists( 'LicenseKeys\Utility\Client' ) ) {
			require_once( 'class-client.php' );
		}
		if ( ! class_exists( 'LicenseKeys\Utility\LicenseRequest' ) ) {
			require_once( 'class-license.php' );
		}
	}

	//= Initiate our class
	$updates = new SSP\SUPER_UPDATES\SUPER_UPDATES( $args );

	//= Load our filters & check the server for updates
	$updates->init();
}
/*
 * End bin
*/
?>