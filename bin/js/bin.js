// JavaScript Document
jQuery(document).ready(function($) {
	"use strict";

	var appParams = spAdminParams;
	
	var validate_license = [];
	validate_license[ appParams.prefix ] = function( prefix, slug, key, method ) {
        var nonce = $( '#' + prefix + '_nonce' ).val();
		$.ajax({
			type: 'POST',
			data: {action: prefix + '_ajax_handler', 'method': method, 'nonce': nonce, 'prefix': prefix, 'slug': slug, 'key': key},
			url: appParams.ajaxurl,
			timeout: 5000,
			error: function( json ) {
				//= Do this is ajax fails
			},
			dataType: 'json',
			success: function( json ) {
				if ( true === json.response.error ) {
					var msg = json.response.errors.license_key;
					$( '.' + appParams.prefix + '-updates-license-msg-' + slug ).html( msg );
					return;
				}
				$( '.' + appParams.prefix + '-updates-license-msg-' + slug ).html( json.response.message );
				//= Do this if ajax works correctly
				if ( 'activate' === json.method ) {
					$( '.' + appParams.prefix + '-validate-license' ).val ( 'Deactivate' );
					$( '#' + slug + '-license' ).addClass( 'active-license' );
				} else {
					$( '.' + appParams.prefix + '-validate-license' ).val ( 'Activate' );
					$( '#' + slug + '-license' ).removeClass( 'active-license' );
				}
				return;
			}
		});
	};

	$( document ).on( 'click', '.' + appParams.prefix + '-validate-license', function() {
        var msg = '';
        var prefix = $( this ).data( 'prefix' );
        var slug = $( this ).data( 'slug' );
        var key = $( '#' + slug + '-license' ).val();
		var method = $( this ).val();
		method = method.toLowerCase();

        if ( '' === key ) {
            //= TO DO - localize
            msg = 'No Key entered';
            $( '.' + appParams.prefix + '-updates-license-msg-' + slug ).html( msg );
            return;
        }

        validate_license[ appParams.prefix ]( prefix, slug, key, method );
	});
	
});