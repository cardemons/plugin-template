<?php
//= https://github.com/10quality/license-keys-php-client/wiki/Tutorial:-Quick-Script
/**
 * Method used to activate the product. Returns true or false.
 * @param string $license_key A customer license key.
 * @return bool
 */
function photomeme_activate( $license_key ) {
    $response = Api::activate(
        Client::instance(),
        function() use( $license_key ) {
            return LicenseRequest::create(
                'https://tutorials.net/wp-admin/admin-ajax.php',
                'fake007',
                'PHOTOMEME',
                $license_key ,
                LicenseRequest::DAILY_FREQUENCY
            );
        },
        function( $license ) {
           update_option( PHOTOMEME_OPTION_KEY, $license, true );
        }
    );
    return $response->error === false;
}

/**
 * Returns true or false.
 * @return bool
 */
function photomeme_is_valid() {
    return Api::softValidate(
        function() {
            return new LicenseRequest( get_option( PHOTOMEME_OPTION_KEY ) );
        }
    );
}

/**
 * Returns true or false.
 * @return bool
 */
function photomeme_deactivate() {
    $response = Api::deactivate(
        Client::instance(),
        function() {
            return new LicenseRequest( get_option( PHOTOMEME_OPTION_KEY) );
        },
        function( $license ) {
           if ($license === null)
               update_option( PHOTOMEME_OPTION_KEY, null );
        }
    );
    return $response->error === false;
}


?>