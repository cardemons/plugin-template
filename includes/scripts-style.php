<?php
function ssp_enqueue_scripts() {
	$is_mobile = wp_is_mobile();
	wp_register_script( 'pt-js', plugins_url() . '/_plugin_template/js/main.js', array( 'jquery' ), PT_VERSION );
	wp_localize_script( 'pt-js', 'scParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'api_url'  => site_url( '/wp-json/_plugin_template_get_items/v1/' ),
        'api_nonce' => wp_create_nonce( 'wp_rest' ),
        'site_url' => get_site_url(),
		'is_mobile' => $is_mobile,
	) );

	wp_enqueue_script( 'pt-js' );
	wp_enqueue_style( 'pt-css', plugins_url() . '/_plugin_template/css/main.css', false, PT_VERSION );
}

add_action( 'wp_enqueue_scripts', 'ssp_enqueue_scripts' );
?>