<?php
function pt_output( $settings ) {
	$default_settings = array( 
		'title' => __( 'My Widget', '_plugin_template' ),
		'class' => '',
		'number_of_posts' => 3,
		'item_header' => '',
		'item_footer' => '',
		'post_id' => false,
	 );
	 $settings = wp_parse_args( $settings, $default_settings );

	$html = '';
	if ( $settings['number_of_posts'] ) {
		$settings['number_to_show'] = $settings['number_of_posts'];
	}

	$args = array(
		'post_type' => 'my_post_type',
		'paged' => true,
		'posts_per_page' => $settings['number_to_show'],
	 );

	if ( $settings['post_id'] ) {
		$args['p'] = $settings['post_id'];
	}

	$query = new WP_Query();
	$query->query( $args );
	
	if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
		$post_id = $query->post->ID;

		$item = '';
		$item .= '<div class="pt-item pt-item-' . $post_id . '" data-post-id="' . $post_id . '">';
			$item .= '<div class="pt-item-header pt-item-header-' . $post_id . '" data-post-id="' . $post_id . '">';
				$item .= pt_header( $post_id, $settings['item_header'] );
			$item .= '</div>';
			$item .= '<div class="pt-item-content pt-item-content-' . $post_id . '" data-post-id="' . $post_id . '">';
				$item .= $content = apply_filters( 'the_content', $query->post->post_content );
			$item .= '</div>';
			$item .= '<div class="pt-item-footer pt-item-footer-' . $post_id . '" data-post-id="' . $post_id . '">';
				$item .= pt_footer( $post_id, $settings['item_footer'] );
			$item .= '</div>';
			$item .= '<div class="pt-clear"></div>';
		$item .= '</div>';
		
		$html .= apply_filters( '_plugin_template_item_filter', $item, $post_id, $settings );

	endwhile; endif;

	//= Technically this shouldn't be needed, but let's call it anyway
	wp_reset_query();
	
	if ( ! empty( $settings['class'] ) ) {
		$settings['class'] = trim( $settings['class'] );
		$settings['class'] = ' ' . $settings['class'];
	}
	
	if ( ! empty( $html ) ) {
		$html = '<div class="pt-items-wrap' . $settings['class'] . '">' . $html . '<div class="pt-clear"></div></div>';
	} else {
		$html = '<div class="pt-items-wrap pt-items-wrap-empty' . $settings['class'] . '"></div>';
	}

	$html = apply_filters( '_plugin_template_html_filter', $html, $settings );
	return $html;
}

function pt_header( $post_id, $content = '' ) {
	$content = str_replace( '{post_id}', $post_id, $content );
	$html = $content;	
	$html = apply_filters( '_plugin_template_item_header_filter', $html, $post_id, $content );
	return $html;
}

function pt_footer( $post_id, $content = '' ) {
	$content = str_replace( '{post_id}', $post_id, $content );
	$html = $content;
	$html = apply_filters( '_plugin_template_item_footer_filter', $html, $post_id, $content );
	return $html;
}
?>