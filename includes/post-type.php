<?php
/**
 * Register coupons post type
 *
 */
function _plugin_template_post_type() {
	register_post_type( 'my_post_type',
		array(
			'labels' => array(
			'name' => __( 'My Post Type','_plugin_template' ),
			'singular_name' => __( 'My Post Type','_plugin_template' ),
			'add_new_item' => __( 'Add New My Post Type', '_plugin_template' ),
			'edit_item' =>  __( 'Edit My Post Type', '_plugin_template' ),
		  ),
		  'public' => true,
		  'rewrite' => array( 'slug' => 'my_post_type', 'with_front' => false, ),
		  'has_archive' => true,
		  'supports' => array( 'title', 'editor', 'custom-fields' ),
		  'menu_icon' => 'dashicons-tag',
		)
	);
}
add_action( 'init', '_plugin_template_post_type' );

function _plugin_template_taxonomies() {
	register_taxonomy(
		'my_post_type_tax',
		'my_post_type',
		array(
			'label' => __( 'My Taxonomy Name', '_plugin_template' ),
			'labels' => array(
			'add_new_item' => __( 'Add New Taxonomy Term', '_plugin_template' ),
			'edit_item' =>  __( 'Edit Taxonomy Term', '_plugin_template' ),
		),
		'public' => false,
		'show_ui' => true,
		)
	);
}
add_action( 'init', '_plugin_template_taxonomies' );
?>