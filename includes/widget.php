<?php
add_action( 'widgets_init', '_plugin_template_widgets' );
function _plugin_template_widgets() {
	register_widget( '_plugin_template_Widget' );
}
class _plugin_template_Widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => '', 'description' => __( 'Describe your widget', '_plugin_template' ) );
		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => '_plugin_template-widget' );
		/* Create the widget. */
		parent::__construct( '_plugin_template-widget', __( '', '_plugin_template'), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */

		echo $args['before_widget'];
		/* Display the widget title if one was input (before and after defined by themes). */
		if (!empty($title)) {
			$title = $args['before_title'] . $title . $args['after_title'];
		}
		echo $title;
		//= Insert Search Form Here ==========================

		// setup the setting to send to cdfi_advanced_search_form()
		echo output( $instance );

		/* After widget (defined by themes). */
		echo $args['after_widget'];
	}
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['class'] = strip_tags( $new_instance['custom_class'] );
		$instance['number_of_posts'] = strip_tags( $new_instance['number_of_posts'] );
		$instance['item_header'] = strip_tags( $new_instance['item_header'] );
		$instance['item_footer'] = strip_tags( $new_instance['item_footer'] );
		$instance['post_id'] = strip_tags( $new_instance['post_id'] );

		return $instance;
	}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 
			'title' => __( 'My Widget', '_plugin_template' ),
			'class' => '',
			'number_of_posts' => 3,
			'item_header' => '',
			'item_footer' => $content,
			'post_id' => false,
		 );

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<div class="cdfi_widget_admin">
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
            </p>
    
            <p>
                <label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e( 'Custom CSS Class:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" value="<?php echo $instance['custom_class']; ?>" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'number_of_posts' ); ?>"><?php _e( 'Number of Items:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'number_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'number_of_posts' ); ?>" value="<?php echo $instance['number_of_posts']; ?>" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'item_header' ); ?>"><?php _e( 'Item header content:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'item_header' ); ?>" name="<?php echo $this->get_field_name( 'item_header' ); ?>" value="<?php echo $instance['item_header']; ?>" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'item_footer' ); ?>"><?php _e( 'Item footer content:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'item_footer' ); ?>" name="<?php echo $this->get_field_name( 'item_footer' ); ?>" value="<?php echo $instance['item_footer']; ?>" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'Show only this Post ID:', '_plugin_template' ); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" value="<?php echo $instance['post_id']; ?>" />
            </p>
    
		</div>
	<?php
	}
}


?>