<?php
add_action( 'wp_ajax__pt_ajax_handler', '_pt_ajax_handler' );
add_action( 'wp_ajax_nopriv__pt_ajax_handler', '_pt_ajax_handler' );
function _pt_ajax_handler() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	$nonce = $_POST['my_nonce'];
	
/*
	FRONT END CODE FOR ADDING A NONCE TO THE PAGE
	//= Add our nonce field
	$nonce = wp_create_nonce( 'my_nonce' );
	$x = '<input type="hidden" name="my_nonce" id="my_nonce" value="' . $nonce . '" />';
	echo $x;
*/
	
	if ( ! wp_verify_nonce( $nonce, 'my_nonce' ) ) {
		echo __( 'Nonce check failed - no changes saved.', 'car-demon' );
		exit();
	}

	$return = array();
	
	$json = json_encode( $return );
	echo $json;
	exit();
}
?>