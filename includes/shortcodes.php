<?php
function _pt_func( $atts, $content ) {
	$atts = shortcode_atts( array(
		__( 'number_to_show', '_plugin_template' ) => '0', /* 0 shows all */
		__( 'number_of_posts', '_plugin_template' ) => false, /* yes, this is a fallback for the one above */
		__( 'class', '_plugin_template' ) => '', /* Add a custom class to the wrapper */
		__( 'item_header', '_plugin_template' ) => '',
		__( 'item_footer', '_plugin_template' ) => $content,
		__( 'post_id', '_plugin_template' ) => false,
	), $atts, '_plugin_template' );

	$html = pt_output( $atts );

	return $html;
}

add_shortcode( __( 'my_shortcode', '_plugin_template' ), '_pt_func' );
?>