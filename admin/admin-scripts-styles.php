<?php
/* Add our admin scripts & styles */
function _pt_admin_custom_scripts() {
	$pluginpath = plugins_url() . '/_plugin_template/';
	wp_enqueue_style( 'pt_admin_css', $pluginpath . 'css/main-admin.css', false, PT_VERSION );
	wp_register_script( 'pt_admin_js', $pluginpath . 'js/main-admin.js', array( 'jquery' ), PT_VERSION );
	wp_localize_script( 'pt_admin_js', 'pt_AdminParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
	) );
	wp_enqueue_script( 'pt_admin_js' );
}
?>