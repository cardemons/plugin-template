<?php
/**
 * Require our file that loads our admin scripts & styles
*/
require_once( 'admin-scripts-styles.php' );

/* Admin init */
add_action( 'admin_init', '_pt_settings_init' );
 
/* Settings Init */
function _pt_settings_init() {
	//= Add our admin scripts & styles
	_pt_admin_custom_scripts();

	//==============================================================
    /* Create settings section */
    add_settings_section(
        'pt-settings-id',                  // Section ID
        'My Admin Settings',				// Section title
        'pt_description',					// Section callback function
        'general'                          	// Settings page slug
    );

	//==============================================================
    /* Register Settings */
	/* ALL SETTINGS MUST BE REGISTERED */
    register_setting(
        'general',       	      	// Options group
        'my_setting_slug',   		// Option name/database
        'pt_settings_sanitize' 		// sanitize callback function
    );

	//============================================================== 
    /* Actually Create settings field */
    add_settings_field(
        'my_setting_slug', 			     		// Field ID
        'My Custom Setting',					// Field title 
        'pt_settings_callback',					// Field callback function
        'general',                    			// Settings page slug
        'pt-settings-id'             			// Section ID
    );

}

function pt_description() {
	$html = __( 'These settings allow you to manage your _A Generic PlugIn Template PlugIn.', '_plugin_template' );
	echo wpautop( $html );
}

/* Sanitize Callback Function */
function pt_settings_sanitize( $input ) {
	return sanitize_text_field( $input );
}

/* Settings Field Callback */
function pt_settings_callback() {
	$setting_slug = 'my_setting_slug';

	$html = '';
	$html .= '<label for="' . $setting_slug . '">';
		$html .= '<input type="text" id="' . $setting_slug . '" name="' . $setting_slug . '" value="' . get_option( $setting_slug, '' ) . '" />';
		$html .= '<br />';
        $html .= '<small>' . __( 'Enter the value for this setting.', '_plugin_template' ) . '</small>';
	$html .= '</label>';

	echo $html;
}

?>