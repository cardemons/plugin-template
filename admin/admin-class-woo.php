<?php

class WC_Settings_Tab_PT_Woo {

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init() {
        add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
        add_action( 'woocommerce_settings_tabs_settings_tab_pt_woo', __CLASS__ . '::settings_tab' );
        add_action( 'woocommerce_update_options_settings_tab_pt_woo', __CLASS__ . '::update_settings' );
    }
    
    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_swu'] = __( 'PlugIn Template', '_plugin_template' );
        return $settings_tabs;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab() {
        woocommerce_admin_fields( self::get_settings() );
    }

    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings() {
        woocommerce_update_options( self::get_settings() );
    }

    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings() {

        $settings = array(
            'section_title' => array(
                'name'     => __( 'PlugIn Template', '_plugin_template' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_settings_tab_pt_woo_section_title'
            ),
            'title' => array(
                'name' => __( 'Redirect URL', '_plugin_template' ),
                'type' => 'text',
                'desc' => __( 'Enter a URL to redirect visitors if they remove the last item from the checkout page. Leave blank to disable.', 'woocommerce-settings-tab-swu' ),
                'id'   => 'wc_settings_tab_swu_redirect'
            ),
            'section_end' => array(
                 'type' => 'sectionend',
                 'id' => 'wc_settings_tab_pt_woo_section_end'
            )
        );

        return apply_filters( 'wc_settings_tab_pt_woo_settings', $settings );
    }

}

WC_Settings_Tab_PT_Woo::init();

?>