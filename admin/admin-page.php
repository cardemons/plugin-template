<?php
//= Admin Functions
//==================
if ( is_admin() ) {
    add_action( 'admin_init', '_plugin_template_admin_init' );
	add_action( 'admin_menu', 'register_plugin_template_menu' );
	add_action( 'admin_enqueue_scripts', '_pt_admin_custom_scripts' );
}

function _plugin_template_admin_init() {
	//= Bail if no activation redirect
	if ( ! get_transient( '_plugin_template_activation_redirect' ) ) {
		return;
	}
	
	//= Delete the redirect transient
	delete_transient( '_plugin_template_activation_redirect' );

	//= Bail if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}
	
	//= Redirect to your admin page
	wp_safe_redirect( add_query_arg( array( 'page' => '_plugin_template_menu_slug', 'post_type' => 'my_post_type' ), admin_url( 'edit.php' ) ) );
}

function register_plugin_template_menu() {
	add_submenu_page( '_plugin_template_menu_slug', 'My Admin Page', 'My Admin Page', 'edit_pages', '_plugin_template_page', '_plugin_template_page' );
}

function _plugin_template_page() {
	echo '<h1>' . __( 'My Admin Page', '_plugin_template_' ) . '</h1>';
	echo '<div class="wrap">';
	
		echo 'WELCOME';
	
	//= DONT FORGET TO USE A NONCE AND VALIDATE THE USER CAPABILITIES WHEN UPDATING SETTINGS
	
	echo '</div>';
}

?>