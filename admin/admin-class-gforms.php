<?php
/**
 * Include Gravity Forms addon framework
*/
GFForms::include_addon_framework();

/**
 * Extend GFAddOn class to add our admin scripts, styles, global settings and admin page
*/
class PT_CLASS extends GFAddOn {
	protected $_version = '1.0';
	protected $_min_gravityforms_version = '1.3';
	protected $_slug = '_plugin_template';
	protected $_path = '_plugin_template/_plugin_template.php';
	protected $_full_path = __FILE__;
	protected $_title = 'Gravity Forms PlugIn Template';
	protected $_short_title = 'PlugIn Template';

	private $settings;

    // ------------ Permissions -----------
    /**
     * @var string|array A string or an array of capabilities or roles that have access to the settings page
     */
    protected $_capabilities_settings_page = array( 'gravityforms_edit_settings' );
    /**
     * @var string|array A string or an array of capabilities or roles that have access to the form settings
     */
    protected $_capabilities_form_settings = array( 'gravityforms_edit_settings' );
    /**
     * @var string|array A string or an array of capabilities or roles that have access to the plugin page
     */
    protected $_capabilities_plugin_page = array( 'gravityforms_edit_settings' );
    /**
     * @var string|array A string or an array of capabilities or roles that have access to the app menu
     */
    protected $_capabilities_app_menu = array( 'gravityforms_edit_settings' );
    /**
     * @var string|array A string or an array of capabilities or roles that have access to the app settings page
     */
    protected $_capabilities_app_settings = array( 'gravityforms_edit_settings' );
    /**
     * @var string|array A string or an array of capabilities or roles that can uninstall the plugin
     */
    protected $_capabilities_uninstall = array( 'gravityforms_edit_settings' );
    private static $_instance = null;

	private $current_export = 'Default';

    public static function get_instance() {
        if ( self::$_instance == null ) {
            self::$_instance = new PT_CLASS();
        }

        return self::$_instance;
    }

    public function init() {
		parent::init();


    }

	/**
	 * Load js for our admin page
	*/
    public function scripts() {
		$path = $this->get_base_url();
		$path = str_replace( 'admin', '', $path );
        $scripts = array(
            array(
                'handle'  => '_plugin_template_admin_js',
                'src'     => $path . '/js/main-admin.js',
                'version' => $this->_version,
                'deps'    => array( 'jquery' ),
                'strings' => array(
                    'ajaxurl'  => admin_url( 'admin-ajax.php' ),
                ),
                'enqueue' => array(
                    array(
                        'admin_page' => array( 'plugin_settings' ),
                        'tab'        => '_plugin_template'
                    )
                )
            ),

            /* extended example
            array(
                'handle'  => 'gfee_calendar_js',
                'src'     => $path . '/js/CalendarPopup.js',
                'version' => $this->_version,
                'deps'    => array( 'jquery' ),
				'callback' => array( $this, 'localize_scripts' ),
                'strings' => array(
                    'ajaxurl'  => admin_url( 'admin-ajax.php' ),
					'jan' => __( 'January', 'car-demon' ),
					'feb' => __( 'February', 'car-demon' ),
					'mar' => __( 'March', 'car-demon' ),
					'apr' => __( 'April', 'car-demon' ),
					'may' => __( 'May', 'car-demon' ),
					'jun' => __( 'June', 'car-demon' ),
					'jul' => __( 'July', 'car-demon' ),
					'aug' => __( 'August', 'car-demon' ),
					'sep' => __( 'September', 'car-demon' ),
					'oct' => __( 'October', 'car-demon' ),
					'nov' => __( 'November', 'car-demon' ),
					'dec' => __( 'December', 'car-demon' ),
					'clear' => __( 'Clear', 'car-demon' ), 
					'close_it' => __( 'Close', 'car-demon' ),
					'plugin_path' => GFEE_PATH,
                ),
                'enqueue' => array(
                    array(
                        'admin_page' => array( 'plugin_settings' ),
                        'tab'        => 'gforms-export-entries'
                    )
                )
            ),
            */

        );

        return array_merge( parent::scripts(), $scripts );
    }

	/**
	 * Load css for our admin page
	*/
    public function styles() {
		$path = $this->get_base_url();
		$path = str_replace( 'admin', '', $path );
        $styles = array(
            array(
                'handle'  => '_plugin_template_admin_css',
                'src'     => $path . '/css/main-admin.css',
                'version' => $this->_version,
                'enqueue' => array(
                    array(
                        'admin_page' => array( 'plugin_settings' ),
                        'tab'        => '_plugin_template'
                    )
                )
            ),

        );

       return array_merge( parent::styles(), $styles );
    }

	/**
	 * Setup global settings
	*/
    public function plugin_settings_fields() {
        return array(
            array(
                'title'  => esc_html__( 'Export Form Entries', '_plugin_template' ),
                'fields' => array(

					//= add our own custom "setting type" for the current export
					array(
						'label' => esc_html__( 'Welcome', '_plugin_template' ),
						'type'  => '_plugin_template_custom_type',
						'name'  => '_plugin_template_settings_welcome',
						'args'  => array(),
					),

                ), //= end 'fields'

            )
        ); //= end return
    }

    public function settings__plugin_template_custom_type( $field, $echo = true ) {
        $x = '';
        $x .= '<div>';
            $x .= '<a href="https://optireto.com/" target="_blank"/>';
                $path = $this->get_base_url();
                $img = str_replace( 'admin', 'images/optireto.png', $path );
                $x .= '<img src="'. $img . '" />';
            $x .= '</a>';
        $x .= '</div>';

        echo $x;
    }
    
	/**
	 * Save our custom settings
	*/
	private function _plugin_template_save_settings() {
		$settings = get_option( '_plugin_template_settings', array() );

        //= SAVE DATA AS NEEDED
        
		update_option( '_plugin_template_settings', $settings, false );
		$this->settings = $settings;
	}

}

/**
 * Register our Gravity Forms add-on
*/
GFAddOn::register( 'PT_CLASS' );

?>