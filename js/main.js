// JavaScript Document

jQuery(document).ready(function($) {
	"use strict";

	var my_ajax_function = function() {
		var my_data = '';
		$.ajax({
			type: 'POST',
			data: {action: '_pt_ajax_handler', 'my_data': my_data},
			url: cdcpParams.ajaxurl,
			timeout: 5000,
			error: function( json ) {
				//= Do this is ajax fails
			},
			dataType: 'json',
			success: function( json ) {
				//= Do this if ajax works correctly
			}
		});
	};
	
    //= Make rest call to set all gfac cookies
	var _plugin_template_get_items = function () {
		var url_parameters = window.location.search;
		url_parameters = url_parameters.replace("?", ''); // remove the ?
		var referer = document.referrer.replace(/\/\s*$/, "");//remove trailing "/"

		var site_url = _plugin_templateParams.site_url;
		//referer could be full url with directories and url parameters and we only care if the domain portion matches our site url 
		if ( referer.indexOf( _plugin_templateParams.site_url ) !== -1 ) {//site_url === referer
			return false;
		}

        // Compile our data object - notice that it is only the form_id.
        // The nonce is sent in a request header - see beforeSend in $.ajax
        // No action is required, we specify the direct endpoint we created above as the url in $.ajax
        var data = {
            url_parameters: url_parameters,
            referer: referer,
        };

        // Fire our ajax request!
        $.ajax({
            method: 'POST',
            // Here we supply the endpoint url, as opposed to the action in the data object with the admin-ajax method
            url: _plugin_templateParams.api_url + '_plugin_template_get_items/', 
            data: data,
            beforeSend: function ( xhr ) {
                // Here we set a header 'X-WP-Nonce' with the nonce as opposed to the nonce in the data object with admin-ajax
                xhr.setRequestHeader( 'X-WP-Nonce', _plugin_templateParams.api_nonce );
            },
            success : function( response ) {
                console.log( response )
                var json = response.json;
                console.log( json );
            },
            fail : function( response ) {
                console.log( '_plugin_template fail' );
                console.log( response );
            },
            statusCode: {
               403: function( response ) { 
                   console.log( '_plugin_template 403' );
                   console.log( response );
               },
            }
        });
        
    }; //= end _plugin_template_get_items()
    
});