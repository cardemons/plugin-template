=== _A Generic PlugIn Template ===  
Contributors:  
Tags: 
Requires at least: 
Tested up to: 
Stable tag: 0.0.1
Bitbucket Plugin URI: BIT_BUCKET_URI
Bitbucket Branch:     master  

== Description ==

Create a simple timed popup

== Installation ==

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the 

Plugin from Plugins page.  

== Usage Instructions ==

Install Instructions

== Changelog ==
= 0.0.1 =
*  
* First release in repository