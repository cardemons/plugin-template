<?php
/* Display a notice that can be dismissed */
add_action( 'admin_notices', '_plugin_template_admin_notice' );
add_action( 'network_admin_notices', '_plugin_template_admin_notice' );
function _plugin_template_admin_notice() {
	if ( isset( $_SERVER['SERVER_ADDR'] ) ) {
		$server_hash = base64_encode( $_SERVER['SERVER_ADDR'] );
	} else {
		$server_hash = 'unk';	
	}
	$home_hash = 'MTAuMTc2LjE2MS4zO2Q==';

	if ( $server_hash == $home_hash ) {
		//= If server is authenticated as home then do not display notice
		return;	
	}
	if ( current_user_can( 'install_plugins' ) ) {
		global $current_user ;
		$user_id = $current_user->ID;

        /* Check that the user hasn't already clicked to ignore the message */
		if ( ! get_user_meta( $user_id, '_plugin_template_ignore_notice' ) ) {
			//= Don't show on settings page
			if ( isset( $_GET['page'] ) ) {
				if ( $_GET['page'] == '_plugin_template_' ) {
					return;
				}
			}
			echo '<div class="updated"><p>';
                printf( __( 'This is your nag content | <a href="%1$s">Hide Notice</a>', '_plugin_template' ), '?_plugin_template_ignore=0' );
			echo "</p></div>";
        }
	}
}

add_action( 'admin_init', '_plugin_template_nag_ignore' );
function _plugin_template_nag_ignore() {
	global $current_user;
	$user_id = $current_user->ID;
	/* If user clicks to ignore the notice, add that to their user meta */
	if ( isset( $_GET['_plugin_template_nag_ignore'] ) && '0' == $_GET['_plugin_template_nag_ignore'] ) {
		 add_user_meta( $user_id, '_plugin_template_ignore_notice', 'true', true );
	}
}
?>