<?php
/**
 * Class MY_CLASS
 *
 * Get remote data from a Bitbucket repo.
 *
 * @package PT\MY_CLASS
 * @author  YOUR NAME
 */

namespace PT\MY_CLASS;

/*
 * Exit if called directly.
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}

class MY_CLASS {

	/**
	 * Setup variables
	*/
	private $my_var;

	/**
	 * Construct the class
	*/
	function __construct( $my_var = false ) {

		//= Do stuff to build our class

	}

	/**
	 * Example private function
	 *
	 * @param $args - array of settings
	*/
	private function my_private_function( $args ) {
		
		return true;
	}

	/**
	 * Example public function
	 *
	 * @param $args - array of settings
	*/
	public function my_public_function( $args ) {
		
		return true;
	}

	/**
	 * Attempt to get request IP
	 * Just a handy function that sometimes comes in use
	 *
	*/
	private function request_ip() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		if ( strpos( $ip, ',' ) !== false ) {
			$ips = explode( ',', $ip );
			$ip = $ips[0];
		}
		
		return $ip;
	}

	/**
	 * Save log items to WordPress debug.log
	 *
	 * @param $log - line to record in log file
	*/
    private function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( 'MY_CLASS: ' . print_r( $log, true ) );
            } else {
                error_log( 'MY_CLASS: ' . $log );
            }
        }
    }

}
?>