<?php
/**
* Plugin Name: _A Generic PlugIn Template
* Plugin URI: http://CarDemon.com/
* Description: This is a generic PlugIn Template
* Author: CarDemons
* Version: 0.0.1
* Author URI: http://CarDemon.com/
* License: GPL2
*/

/**
 * Return 403 error if loaded directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

define( 'PT_VERSION', '0.0.1' );

/**
 * Require our file that handles adding a license key requirement for updates
 * Function for usage beyond providing updates can be found in bin/class-config.php
*/
require_once( 'bin/load.php' );

/**
 * Require our file that creates an admin area on the General settings page
 * If you have a lot of settings then consider using admin-page.php and removing this file
*/
require_once( 'admin/admin-settings.php' );

/**
 * Require our file that creates a dedicated admin page
 * If you don't have a lot of settings then consider using admin-settings.php and remove this file
*/
//require_once( 'admin/admin-page.php' );

/**
 * Require our file that creates a dedicated Gravity Forms admin page
*/
//require_once( 'admin/admin-class-gforms.php' );

/**
 * Require our file that creates a dedicated Woocommerce admin page
*/
//require_once( 'admin/admin-class-woo.php' );
 
/**
 * Require our file to create a nag notice in the admin area of the site
*/
require_once( 'nag/nag.php' );

/**
 * Require our file to create our custom post type
*/
require_once( 'includes/post-type.php' );

/**
 * Require our file that loads our front end scripts & styles
*/
require_once( 'includes/scripts-style.php' );

/**
 * Require our file that handles our ajax requests
*/
require_once( 'includes/ajax-handler.php' );

/**
 * Require our file that handles our REST api requests
*/
require_once( 'rest/rest-routes.php' );

/**
 * Require our file that creates a class
*/
require_once( 'classes/my-class.php' );

/**
 * Require our file that outputs the results for our shortcode(s), widget(s) & block(s)
*/
require_once( 'includes/output.php' );

/**
 * Require our file that handles our shortcode(s)
*/
/*
=========================================================
PRO TIP: If you have more than one shortcode then create a folder called "shortcodes".
	Then put the code for each one in it's own file and require them from the includes/shortcodes.php file
	Do the same thing with widgets and blocks but name their folders respectively.
=========================================================
*/
require_once( 'includes/shortcodes.php' );

/**
 * Require our file that handles our widget(s)
*/
require_once( 'includes/widget.php' );

/**
 * Require our file that create our Gutenberg block(s)
*/
require_once( 'blocks/blocks.php' );

/**
 * Run activation
 *
 * Set default options
 *
 * Set welcome screen transient
 */
function _plugin_template_activate() {
    //= Set a transient and use it to force the user to go to the admin page after activation
    set_transient( '_plugin_template_activation_redirect', true, 30 );

    //= register post type and flush rewrite rules
	_plugin_template_post_type();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, '_plugin_template_activate' );

/**
 * Run deactivation
 *
 * Do not delete settings
 *
 * @todo
 *     - add option to delete settings and post data
 */
function _plugin_template_deactivate() {

}
register_deactivation_hook( __FILE__, '_plugin_template_deactivate' );

/**
 * Load Localisation files.
 *
 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
 *
 */
function _plugin_template_language(){
    $domain = '_plugin_template';
    // The "plugin_locale" filter is also used in load_plugin_textdomain()
    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

    load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename(__FILE__) ) . '/languages/' );
}
add_action( 'plugins_loaded', '_plugin_template_language' );

?>