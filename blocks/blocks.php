<?php
function pt_block_init() {
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}
	
	//= Load our front end styles into the admin area so Gutenberg can load them
	ssp_enqueue_scripts();

	$dir = dirname( __FILE__ );
	$js = '/js/blocks.js';
	wp_register_script(
		'pt-blocks-js',
		plugins_url( $js, __FILE__ ),
		array( 'pt-js', 'wp-element', 'wp-components', 'wp-editor' ),
		PT_VERSION
	);

	//= Add any js labels you need to it's own array
	$js_strings = array(
		'block_title' => __( 'My Block Template', '_plugin_template' ),
		'class' => __( 'Custom CSS Class', '_plugin_template' ),
		'number_of_posts' => __( 'Number of Items', '_plugin_template' ),
		'item_header' => __( 'Item Header', '_plugin_template' ),
		'item_footer' => __( 'Item Footer', '_plugin_template' ),
		'post_id' => __( 'Show only this post id', '_plugin_template' ),
	);

	wp_localize_script( 'pt-blocks-js', 'pt_BlocksParams', array(
		'strings' => $js_strings,
	) );
	
	wp_enqueue_script( 'pt-blocks-js' );

	register_block_type( 'plugin-template/pt-block-name', array(
		'editor_script' => 'pt-blocks',
		'attributes' => array(
			'block_title' => array(
				'type' => 'string',
			),
			'class' => array(
				'type' => 'string',
			),
			'number_of_posts' => array(
				'type' => 'string',
			),
			'item_header' => array(
				'type' => 'string',
			),
			'item_footer' => array(
				'type' => 'string',
			),
			'post_id' => array(
				'type' => 'string',
			),
		),
		'render_callback' => 'pt_output'
	) );

}
add_action( 'init', 'pt_block_init' );
?>