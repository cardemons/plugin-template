// JavaScript Document
var el = wp.element.createElement,
	registerBlockType = wp.blocks.registerBlockType,
	ServerSideRender = wp.components.ServerSideRender,
	TextControl = wp.components.TextControl,
//	TextareaControl = wp.components.TextareaControl,
//	SelectControl = wp.components.SelectControl,
	InspectorControls = wp.editor.InspectorControls;

/* Icon for Block */
var pt_BlockIcon = el ("img", {
	src: "/wp-content/plugins/_plugin_template/blocks/images/calculator.svg",
	width: "24px",
	height: "24px"
});
console.log( pt_BlocksParams );
/* Block for _plugin_template/pt-block-name */
registerBlockType( 'plugin-template/pt-block-name', {
	title: pt_BlocksParams.strings.block_title,
	category: 'embed',
	icon: {
		src: pt_BlockIcon
	},

	edit: function( props ) {
		var atts = [
			el( ServerSideRender, {
				block: 'plugin-template/pt-block-name',
				attributes: props.attributes,
			} ),
			el( InspectorControls, {},
				el( TextControl, {
					label: pt_BlocksParams.strings.class,
					value: props.attributes.class,
					onChange: ( value ) => { props.setAttributes( { class: value } ); },
				} ),
				el( TextControl, {
					label: pt_BlocksParams.strings.number_of_posts,
					value: props.attributes.number_of_posts,
					onChange: ( value ) => { props.setAttributes( { number_of_posts: value } ); },
				} ),
				el( TextControl, {
					label: pt_BlocksParams.strings.item_header,
					value: props.attributes.item_header,
					onChange: ( value ) => { props.setAttributes( { item_header: value } ); },
				} ),
				el( TextControl, {
					label: pt_BlocksParams.strings.item_footer,
					value: props.attributes.item_footer,
					onChange: ( value ) => { props.setAttributes( { item_footer: value } ); },
				} ),
				el( TextControl, {
					label: pt_BlocksParams.strings.post_id,
					value: props.attributes.post_id,
					onChange: ( value ) => { props.setAttributes( { post_id: value } ); },
				} )
/*
				el( SelectControl, {
					label: pt_BlocksParams.strings.yes_no_example,
					value: props.attributes.yes_no_example,
					options: [
						{ label: pt_BlocksParams.strings.no, value: '' },
						{ label: pt_BlocksParams.strings.yes, value: pt_BlocksParams.strings.yes },
					],
					onChange: ( value ) => { props.setAttributes( { yes_no_example: value } ); },
				} )
*/
			),
		];

		return atts;
	},
	
	save: function(props) {
		return null;
	}
});