<?php
add_action( 'rest_api_init', '_plugin_template_rest_api_init' );
function _plugin_template_rest_api_init() {
    $namespace = '_plugin_template/v1';

    $route = '/_plugin_template_get_items/';
    register_rest_route( $namespace, $route, array(
        'methods'  => 'POST',
        'callback' => '_plugin_template_get_items',
        'args' => array(
            'page_url'  => array( 'required' => true ), // This is where we could do the validation callback
        ),
    ) );
}

function _plugin_template_get_items( $request ) {
    //= We don't need to specifically check the nonce like with admin-ajax. It is handled by the API.
    $params = $request->get_params();
    
	$return = array();

	if ( isset( $_POST['params'] ) ) {
		$page_url = sanitize_text_field( $params['page_url'] );
		$post_id = url_to_postid( $page_url );
		$return['post_id'] = $post_id;
		$return['page_url'] = $page_url;

	} else {
		$return['error'] = __( 'Page Unknown', '_plugin_template' );
	}

	$json = json_encode( $return );

    $msg = __( 'API Items have been loaded', '_plugin_template' );
    return new WP_REST_Response( array( 'message' => $msg, 'json' => $json ), 200 );
}

?>